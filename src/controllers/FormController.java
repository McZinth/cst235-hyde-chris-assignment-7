package controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ejb.EJB;

/**
 * @author Christopher Hyde
 * @dueDate December 2, 2018
 * @Class CST-235
 * @Instructor Mark Smithers
 * 
 * @about This class is the JSF Managed bean controlling the form for
 * 			assignment2b.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.Order;
import beans.User;
import business.MyTimerService;
import business.OrdersBusinessInterface;

@ManagedBean( name = "FormController", eager = true)
@RequestScoped
public class FormController {
	
	@Inject()
	private OrdersBusinessInterface service;
	
	@EJB
	private MyTimerService timer;
	
	private Connection conn = null;
	private String query = "SELECT * FROM TESTAPP.ORDERS";
	private String insert = "INSERT INTO  TESTAPP.ORDERS(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY)VALUES('001122334455', 'This was inserted new', 25.00, 100)";

	/**
	 * Method for controlling the Submit commandButton on TestForm.xhtml
	 * @param user - User Managed Bean
	 * @return String - url
	 */
	public String onLogOff(User user) {
		
		// Invalidate the Session to clear the security token
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			
		// Redirect to a protected page (so we get a full HTTP Request) to get Login Page
		return "TestResponse.xhtml?faces-redirect=true";

	}
	
	/**
	 * Method for controlling the Flash commandButton on TestForm.xhtml
	 * @param user - User Managed Bean
	 * @return String - url with redirect query
	 */
	public String onFlash(User user) {
		
		//Set the 'user' attribute to FacesContext request Map
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("User", user);	
		return "TestResponse2.xhtml?faces-redirect=true";
	}
	
	//Connection to the database using a JDBC 
	private void getAllOrders() {
		try {
			conn = DriverManager.getConnection("jdbc:derby:/Users/Chris/myDB/TestappDS","user", "derby");
			System.out.println("DATABASE CONNECTION: SUCESSFUL!");
			
			//Query the database
			try {
			System.out.println("DATABASE QUERY: STARTED!");
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				System.out.print("ID: " + rs.getInt("ORDER_NO") + ", ");
				System.out.print("PRODUCT_NAME: " + rs.getString("PRODUCT_NAME") + ", ");
				System.out.print("PRICE: $" + rs.getFloat("PRICE") + "\n");
			}
			
			rs.close();
			stmt.close();
			
			}catch(SQLException e) {
				System.out.println("DATABASE QUERY: UNABLE TO PERFORM QUERY!");
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			System.out.println("DATABASE CONNECTION: FAILED TO CONNECT TO jdbc:derby:/Users/Chris/myDB/TestappDS");
			e.printStackTrace();
		}finally {
			//Close the connection to the database if it exists
			if(conn != null) {
				try {
					System.out.println("DATABASE CONNECTION: CLOSING!");
					conn.commit();
					conn.close();
				} catch (SQLException e) {
					System.out.println("DATABASE CONNECTION: FAILED TO CLOSE!");
					e.printStackTrace();
				}
			
			}else {
				System.out.println("DATABASE CONNECTION: NEVER ESTABLISHED!");
				
			}
		}
	}

	private void insertOrder() {
		try {
			conn = DriverManager.getConnection("jdbc:derby:/Users/Chris/myDB/TestappDS","user", "derby");
			System.out.println("DATABASE CONNECTION: SUCESSFUL!");
			
			//Query the database
			try {
			System.out.println("DATABASE QUERY: INSERTION STARTED!");
			Statement stmt = conn.createStatement();
			
			//Insertion
			stmt.executeUpdate(insert);
			
			stmt.close();
			
			}catch(SQLException e) {
				System.out.println("DATABASE QUERY: UNABLE TO INSERT PRODUCT!");
				e.printStackTrace();
			}
			
		} catch (SQLException e) {
			System.out.println("DATABASE CONNECTION: FAILED TO CONNECT TO jdbc:derby:/Users/Chris/myDB/TestappDS");
			e.printStackTrace();
		}finally {
			//Close the connection to the database if it exists
			if(conn != null) {
				try {
					System.out.println("DATABASE CONNECTION: CLOSING!");
					conn.commit();
					conn.close();
				} catch (SQLException e) {
					System.out.println("DATABASE CONNECTION: FAILED TO CLOSE!");
					e.printStackTrace();
				}
			
			}else {
				System.out.println("DATABASE CONNECTION: NEVER ESTABLISHED!");
				
			}
		}
	}
	
	public String onSendOrder() {
		Order test = new Order("ET0051", "Wizard Wars", 15.99f, 5);
		service.sendOrder(test);
		
		return "OrderResponse.xhtml";
	}
	
	//--- Getters & Setters ---
	public OrdersBusinessInterface getService() {
		return service;
	}
	
	
}
