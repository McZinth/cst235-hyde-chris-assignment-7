package business;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import beans.Order;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative()
public class OrdersBusinessService implements OrdersBusinessInterface {

	//Class Scoped Properties
	private List<Order> orders;
	
	@Resource(mappedName="java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;

	@Resource(mappedName="java:/jms/queue/Order")
	private Queue queue;

	
    /**
     * Default constructor. 
     */
	public OrdersBusinessService() {
		// Implement dummy order list
		// Array of dummy orders.
		Order[] dummyOrders = new Order[] { new Order("E1234", "AlphaTech TV", 139.99f, 1),
				new Order("LT321", "Acme Catapult", 49.99f, 1), new Order("BEV01", "Diet Xola", 7.49f, 5),
				new Order("U5982", "Poxie Paper Plates", 4.99f, 10), new Order("ET0051", "Wizard Wars", 15.99f, 5)};

		// Initialize orders with a dummyOrder array
		orders = Stream.of(dummyOrders).collect(Collectors.toList());
	}

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
        System.out.println("Hello from the OrdersBusinessService");
    }

    /**
     * @return List<Order> orders
     */
	public List<Order> getOrders() {
		return this.orders;
	}

	/**
	 * @param List<Order> orders to set List<Order> orders
	 */
	public void setOrders(List<Order> orders) {
		this.orders = orders;
		
	}

	@Override
	public void sendOrder(Order order) {
		// Send a Message for an Order
				try 
				{
					Connection connection = connectionFactory.createConnection();
					Session  session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
					MessageProducer messageProducer = session.createProducer(queue);
					TextMessage message = session.createTextMessage();
					ObjectMessage message2 = session.createObjectMessage();
					message.setText("This is test message");
					messageProducer.send(message);
					messageProducer.send(message2);
					connection.close();
				} 
				catch (JMSException e) 
				{
					System.out.println("SENDORDER: SOMETHING WENT WRONG SENDING ORDER!");
					e.printStackTrace();
				}

		
	}

}
