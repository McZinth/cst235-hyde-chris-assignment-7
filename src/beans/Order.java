package beans;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Christopher Hyde
 * @dueDate December 2, 2018
 * @Class CST-235
 * @Instructor Mark Smithers
 * 
 * @about This class to represent an order.
 */


@XmlRootElement(name = "Order") 
public class Order {
	
	//Properties
	private String orderNumber = "";
	private String productName = "";
	private float price = 0f;
	private int quantity = 0;
	
	public Order() {
		//Set all properties to a dummy 
		this.orderNumber = "dummyNum";
		this.productName = "dummyProduct";
		this.price = 0f;
		this.quantity = 0;
	}
	
	/**
	 * Constructor: To set all property values
	 * @param String: orderNumber the orderNumber to set
	 * @param String: productName the productName to set
	 * @param float: price the price to set
	 * @param int: quantity the quantity to set
	 */
	public Order(String orderNumber, String productName, float price, int quantity) {
		//Set all properties to either blank or 0; 
		this.orderNumber = orderNumber;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}

	// --- Getters and Setters ---
	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
	
}
