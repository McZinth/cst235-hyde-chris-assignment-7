package beans;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Christopher Hyde
 * @dueDate December 2, 2018
 * @Class CST-235
 * @Instructor Mark Smithers
 * 
 * @about This class is a managed bean to handle orders.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name="Orders")
@ViewScoped()
public class Orders {

	
	
}
