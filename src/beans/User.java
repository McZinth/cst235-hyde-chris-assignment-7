package beans;

import java.security.Principal;

import javax.annotation.PostConstruct;

/**
 * @author Christopher Hyde
 * @dueDate December 2, 2018
 * @Class CST-235
 * @Instructor Mark Smithers
 * 
 * @about This class is the JSF Managed bean controlling the user properties for
 * 			assignment2b.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean( name = "User", eager = true)
@SessionScoped
public class User {
	
	//Properties with validation annotation
	@NotNull()
	@Size(min=5, max=15)
	private String firstName;
	
	@NotNull()
	@Size(min=5, max=15)
	private String lastName;
	
	/**
	 * Default Constructor
	 */
	public User() {
		//Set properties default value
		
		this.firstName = "Chris";
		this.lastName = "Hyde";
	}
	
	@PostConstruct 
	public void init() {
		// Get the logged in Principle
		Principal principle= FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
			if(principle == null)
			{
				setFirstName("Unknown");
				setLastName("");
			}
			else
			{
				setFirstName(principle.getName());
				setLastName("");
			}

	}
	
	//--- Getters and Setters ---
	
	/**
	 * Getter for firstName property
	 * @return String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Setter for firstName property
	 * @param String
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter for lastName property
	 * @return String
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Setter for lastName property
	 * @param String
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
	

}
